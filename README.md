# Sailmates Website

These are the sources for the Sailmates association website.
Sailmates' main objective is the support and promote of the development of free and open source operating systems for mobile platforms.

# License
Content on this site is licensed under GNU FDL as written in LICENSE file.

### Site Generator: HuGo

https://gohugo.io/getting-started/quick-start/

### Theme

https://github.com/monkeyWzr/hugo-theme-cactus

### Run the Server local

Clone Repository.

```
git submodule init
git submodule update
```

In Repo run:
```
hugo server
```

## Edit

The content files are in the `content` folder. There, the markdown files have to be edited.
New `config.toml` holds the main config and structure of the page.


## Deploy

TODO, currently manual step to copy to pages repository.