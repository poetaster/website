---
title: Frequently Asked Questions
date: 2023-02-06 22:00:00
---

Sailmates is a non-profit association that aims to support and promote free and open source mobile operating system alternatives.

- Where is Sailmates based?

Sailmates is an Austrian association with an international scope. This means
other Sailmates antennas can be created, for example Sailmates Bolivia or Sailmates
Japan.

- If the association makes a merge request to Sailfish OS, Jolla Oy has to accept it?

No. Sailmates has no link to Jolla Oy. The association has no influence over
  Jolla Oy or the Saifilsh Operating system. The association's member merge request acceptance is up
to Jolla Oy.

- If I give donate money to Sailmates or join the association, I become a shareholder
  of Jolla Oy?

No. The association's goal is not to finance Sailfish OS. As a member or donator
you will be supporting the environment around Sailfish OS or other operating
systems.

- The association is only dedicated to Sailfish OS?

No. The association is not dedicated ONLY to Sailfish OS. There is no mention of
Sailfish OS in our statutes. Projects could be suggested, it would be great if
they can benefit all mobile operating systems.


- You mention open source mobile operating sources in the statutes but Sailfish
  OS is not completely open source

Indeed Sailfish OS is not 100% open source, we would love to help making it more
open source.

- I donated to the association, can I declare my donation in my taxes to
  get some money back from the government?

Unfortunately Sailmates does not fall yet in the requirements for deductible
donations. But this is a longterm goal.

- Can I post a Sailmates topic on the Sailfish OS forum?

Please refrain from doing so. If you have questions you can ask us by e-mail
(contact@sailmates.net). If you wish to talk with other members you can join the
Matrix chat once you are a member. We wish to keep the Sailfish OS forum only
for Sailfish OS topics.
