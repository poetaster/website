---
title: Membership
date: 2022-12-10 16:56:54
categories: verein
keywords:
---

Please find below the steps to join the Sailmates association. We require an
e-mail address, as it is the official means of communication, and optionally a
Matrix username. The Matrix username has to be set on your side, as the
association does not host members accounts.


1. The annual membership fee is €42. You can use a classic wire transfer to make the payment:

```
Account holder: Sailmates
BIC: TRWIBEB1XXX
IBAN: BE65 9674 8601 2196
Wise's address: Avenue Louise 54, Room S52
Brussels
1050
Belgium
```

2. If you chose wire transfer method, apply for a membership by contacting
[contact@sailmates.net](contact@sailmates.net). Please provide us your payment
reference (you can find this in the wire transfer information at your bank) and
optionally, your Matrix username.

Or you can use Assoconnect.com, the membership is €46,20 due to the payment
fees, we do not control them.

[![assoconnect button](./assoconnect_logo.png)](https://sailmates.assoconnect.com/collect/description/295256-t-annual-membership)

3. If you sent us your Matrix username, you'll be added to the Sailmates Matrix
chat.

We accept donations on Liberapay. *Donations do not give access to a membership*:

[![liberapay donation button](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Sailmates/donate)


### Frequent Questions

- "I prefer a monthly fee rather than an annual fee, how can I proceed?"

1. Use Assoconnect.
2. Pick the "Pay the first installment and the rest later" on checkout:
![](./assoconnect_multiple_payments.png)

- "Why should I join the Matrix chat?"

The Matrix chat allows members to communicate with other members without
jeopardizing the Sailfish OS forum.

- "What if I don't want to join the Matrix chat? Will I get any information?"

Yes. The official communication for general assemblies is done by email and you
can still visit the website to follow the topics to be discussed before a
general assembly.

- "You are an Austrian association, why is the IBAN from Belgium? (BE)"

Because opening a bank account for an association in Austria with international
members and board members is too complicated.

- "Why can't I use Liberapay for the membership?"

Liberapay is not meant for this and the management would be too complicated.

- "How long does the membership last?"

One year starting from the first day of subscription.
