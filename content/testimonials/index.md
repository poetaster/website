---
title: Testimonials
date: 2022-12-28
categories: verein
keywords: testimonials
---

Below you will find the testimonials of different SailfishOS users in their native language.

* [German]

	* Als Schüler habe ich mich intensiv mit Datenschutz und Sicherheit von mobilen Betriebssystemen auseinandergesetzt. Das Ergebnis: ich kaufte mir ein Jolla-Phone. Meine erste App schrieb ich, um einzelne Apps auf meinem Smartphone mit einer zusätzlichen PIN zu versehen. So waren meine Chats und Bilder vor Klassenkameraden geschützt, falls jemand die PIN von meinem Handy erspähen sollte. Ganz simple habe ich die Desktop-Dateien von Anwendungen geändert, um vor dem Start einer App eine extra PIN-Abfrage durchzuführen.
	 Später habe ich einen eigenen Jabber-Server betrieben und das Fehlen von OMEMO-Verschlüsselung hat mich dazu gebracht, für vorhandene Python-Bibliotheken ein Frontend zu schreiben. Das Risiko, dass ich durch Unwissen eine Sicherheitslücke einbaute, war mir als Schüler aber doch zu groß und so hat es nie eine Veröffentlichung gegeben. Privat habe ich die App aber genutzt.
	 Während meiner Schulzeit waren die Tarife meines Handyvertrages so schlecht, dass ich den Netzwerkmanager connman mit Support für Internet via USB kompiliert, konfiguriert und auch erfolgreich auf meinem Jolla 1 mit einem Service zum Laufen brachte.
	 Insgesamt bin ich sehr zufrieden, SailfishOS als OS auf meinem Smartphone zu haben. Auch wenn die Aktualisierungen meines Jolla 1 das eine oder andere mal schiefgegangen sind. Das lag immer daran, dass irgendein Patch noch aktiv war oder ein Repository nicht korrekt deaktiviert. Fazit: das Lesen der Aktualisierungshinweise kann einem viele Stunden nervenaufreibende Arbeit ersparen.

* [French]

	* Ma première expérience avec Sailfish OS était le développement de ma première application. Je voulais créer une application qui me serait utile au quotidien. J’ai donc décidé de faire une appli de traduction en utilisant l’API DeepL.
Comme cette application était ma première application, je ne savais pas comment m’y prendre. J’ai donc choisi de regarder les codes disponibles sur le GitHub d’applications SFOS. J’ai finalement préféré m’en tenir à la formule officielle, c’est a dire, lire la documentation sur le site de Sailfish (https://docs.sailfishos.org/Develop/Apps/).
Créer cette appli a été un partage d’émotions entre la frustration et l’incompréhension  d’un code qui ne marche pas et la joie de publier l’application pour que d’autres puissent l’utiliser. J’ai aussi été très content lorsque qu’un autre utilisateur ses proposé de traduire l’application en Suédois ! Par la suite d’autres personnes se sont proposés pour faire de la traduction de l’interface de l’application, mais aussi de m’aider à rajouter des fonctionnalités.
Il faut faire attention car dès que c’est publié pour la première fois on ne s’arrête plus après, on veut rajouter de nouvelles fonctionnalités, la rendre plus rapide, mieux gérer les erreurs..bref, la perfectionner. Un conseil: il faut lire la doc et ne pas faire des pauses de plusieurs mois entre chaque développement.

------

*My first experience with Sailfish OS was developing my first application. I wanted to create an application that would be useful in my daily life. So I decided to make a translation app using the DeepL API.
Since this was my first application, I didn't know how to go about it. So I chose to look at the codes available on the SFOS applications GitHub. I finally preferred to stick to the official way, that is to say, to read the documentation on the Sailfish website (https://docs.sailfishos.org/Develop/Apps/).
Creating this application was a sharing of emotions between the frustration and the incomprehension of a code that doesn't work and the joy of publishing the application so that others can use it. I was also very happy when another user offered to translate the application into Swedish! Later on, other people offered to translate the interface of the application, but also to help me to add features.
You have to be careful because as soon as it is published for the first time, you don't stop afterwards, you want to add new features, make it faster, better manage errors...in short, improve it. A piece of advice: you have to read the docs and don't take breaks of several months between each development.*

* [Romanian]

	* Am început oficial povestea cu Sailfish OS când a fost anunțat JollaC. Înainte de asta, am folosit tot felul de alte telefoane mobile, dar nici unul nu era Linux (nici Android).
După o creștere mică în numărul de aplicații Android pe care le foloseam cu Jolla C, am început să mă deplasez în direcția opusă, folosind din ce în ce mai puține până când am putut să mă transfer la o versiune gratis de Sailfish OS portată pentru Xiaomi Mi4, care nu avea niciun fel de sprijin pentru aplicații Android.
Să folosesc telefonul ca ș PC-ul personal cu Linux a devenit un hobby de atunci. Ca și mai de mult când trebuia să alege componentele PC foarte atent ca să poți să folosești Linux peel, acum trebuie să aleg foarte atent la ce servicii mă abonez în așa fel încât să pot să folosesc Sailfish OS fără suport de Android.

----

*I've officially joined the Sailfish story when JollaC was announced. Before that, I used different other mobile platforms but none of them were Linux (nor Android, for that matter).
After a small uptick in installing and using Android apps on Jolla C I've started to move in the opposite direction, using less and less until I was able to use a free Sailfish OS port for Xiaomi Mi4 which didn't have any Android support whatsoever.
Using the phone as I would use the personal Linux PC has become a hobby since
then. Just like back in the days where you had to carefully pick hardware for it
to be able to boot Linux, now I have to carefully pick which services I
subscribe to so that I am able to boot Sailfish OS without Android support.*
