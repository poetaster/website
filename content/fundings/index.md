---
title: Fundings
date: 2023-03-08 10:00:00
---

Bellow you will find a list of funding programs suitable for open source,
mobile environments. Some funds are mentioned several times due to different
topics or different application periods.

|Name|Application period|Website|Restriction|
|:---|:---|:---|:---|
|Prototype Fund,round 14|01/02-31/03/2023|https://prototypefund.de|German tax payer only|
|Prototype Fund,round 15|01/08-31/09/2023|https://prototypefund.de|German tax payer only|
|Prototype Fund,round 16|01/02-31/03/2024|https://prototypefund.de|German tax payer only|
|Open Technology Fund|*|https://www.opentech.fund||
|netidee|03/01-07/10/2023|https://www.netidee.at|Austrian address|
|SIDN Fonds|*|https://www.sidnfonds.nl|Primary impact in the Netherlands|


## Prototype Fund
### Requirements
* Application must be submitted in German.
* Applicant must be a natural person. 
* Applicant must live and pay taxes in Germany.
* Project has to be implemented within 6 months.
* Project can't be double funded.

### Links
FAQ - https://prototypefund.de/en/apply/faq/

## Open Technology Fund
### Requirements
* No specific requirements except that it matches the OTF's vision:
	* Access to the Internet, including tools to circumvent website blocks, connection blackouts, and widespread censorship;
	* Awareness of access, privacy, or security threats and protective measures, including how-to guides, instructional apps, data collection platforms, and other efforts that increase the efficacy of Internet freedom tools;
	* Privacy enhancement, including the ability to be free from repressive observation and the option to be anonymous when accessing the Internet; 
	* Security from danger or threat when accessing the Internet, including encryption tools.
* Application should be submitted in English (not a must)
* Also suitable for fellowships

### Links
FAQ - https://guide.opentech.fund/faq

## netidee
### Requirements
* Natural persons/associations with an Austrian residential address
* Preferably apply in German but can be in English too (the website/documents
* are in German only)

### Links
FAQ - https://www.netidee.at/faq
Project documents page - https://www.netidee.at/einreichen/projekt

## SIDN Fonds
### Requirements
* Project has a primary impact in the Netherlands
* Project did not start yet
* Project is shorter than 6 months
* Can be done by natural persons or organisations

### Links
FAQ - https://www.sidnfonds.nl/faq
