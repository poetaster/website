---
title: About
date: 2022-12-08 22:00:00
---

Sailmates is a non-profit association aiming to support and promote free and open source mobile operating system alternatives.

See also the [statute](/statutes) document.

