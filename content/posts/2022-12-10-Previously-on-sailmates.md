---
title: Previously on Sailmates 2022
date: 2022-12-16 16:56:54
categories: verein
keywords:
---

- Sep 07 Jojo starts searching how to achieve the "Interest to create a cooperative" for the [Sailfish OS forum topic](https://forum.sailfishos.org/t/interest-to-create-a-cooperative/10799/63).
- Oct 04 Initial contact and founding members are joining the Telegram group.
- Oct 08 Documents are signed at the [SFOS Vienna meetup](https://forum.sailfishos.org/t/sailfish-community-news-20th-october-community/13306#the-austrian-sailfish-meeting-4)
- Nov 11 Association is official in Austria.
- Dec 02 Bank account is official.
- Dec 07 Domain sailfish.net registered